Jianfei Gao
===========

**Email**:bittomit@gmail.com  
**Tel**:(+86)150-2662-8735  

## EDUCATION

- **Shanghai Jiaotong University**, China  
	September, 2013 ~ Present  

     - **Major GPA:** 86.0 / 100  
     	- Freshman Major GPA  
     		1st Semester: 85.9 / 100  
        	2nd Semester: 86.1 / 100  
     	- Sophomore Major GPA  
     		1st Semester: 88.7 / 100  
        	2nd Semester: 82.0 / 100  
     	- Junior Major GPA  
     		1st Semester: 88.9 / 100  

## RESEARCH EXPERIENCE

- **Speech Lab at Shanghai Jiaotong University**, China  
	August, 2015 ~ Present  

	- Developing an speech synthesis APP on Android based on HMM(Hidden Markov Model) voice synthesis technology  
	- Attending VC(Voice Conversion) Challenge 2016 and working on the voice auto-recognization part  
	- Working on the Singing Synthesis Technology till now  

## TEACHING EXPERIENCE

- **Teaching Assistant in MS105, Data Struture**  
	March, 2015 ~ July, 2015  
- **Teaching Assistant in MS106, Principle and Pratice of Computer Algorithm**  
	July, 2015 ~ August, 2015  

## Projects

- **C Compiler** A project for my compiler course, in which I built a simplified compiler for C without any tools  
- **CPU** A project for my system Ⅰ course, in which I built a simple 5-pipeline CPU in Verilog  

## HONORS AND AWARDS

- **NOI(National Olympiad in Informatics) Bronze Medal**, 2011  
- **Academic Excellence Scholarship of Shanghai Jiao Tong University**, 2014  
- **Academic Excellence Scholarship of Shanghai Jiao Tong University**, 2015  

## ADDITION

- Second debater in Zhiyuan College Debate team, 2013  
- Vice-chairman in Science Association of Zhiyuan College, 2015  
- Programming Language  
	C, C++, Python, Pascal, Java, Markdown, Latex, Assembly, Verilog HDL, Matlab  
- Hobby  
	Guitar, Writing(in Chinese)  

