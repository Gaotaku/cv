<center>
<font size = 12>
**Jianfei Gao**
</font>
</center>

<span style="float:left">**Email**: gaotaku@sjtu.edu.cn</span><span style="float:right">**Tel**: (+86) 150-2662-8735</span>

## Intention

I'm interested in Nature Language Processing and Speech Synthesis & Recognition.

## EDUCATION

- **Shanghai Jiao Tong University**
	Sep. 2013 ~ Preset

	ACM Honored Class, Zhiyuan College, **Major GPA:** 86.0 / 100


## HONORS AND AWARDS
- **Academic Excellence Scholarship**, 2015
- **Academic Excellence Scholarship**, 2014
- **NOI (National Olympiad in Informatics), Bronze Medal**, 2011
- **NOIP (National Olympiad in Informatics in Province), First Prize**, 2010

## RESEARCH EXPERIENCE

- **Speech Lab at Shanghai Jiao Tong University**
	August, 2015 ~ Present

	1. Participated in the development of a HMM-baed speech synthesis APP on Android. And I took rolls in the baseline designment of client, part of the synthesis system coding and corpus training for the synthesis system for server.
	2. Attended VC(Voice Conversion) Challenge 2016, worked partly on the voice auto-recognization and gave some advices to enhance the results, and got a result of rank 3rd in overall, and 1st in the part of man-to-man conversion.
	3. Based on the former HMM-based speech synthesis system, I am now developing a Singing Synthesis System after the collection of singing data.

## Projects

- **C Compiler** A project for my compiler course, in which I built a simplified compiler for C without tools.
- **CPU** A project for my system Ⅰ course, in which I built a simple 5-pipeline CPU in Verilog

## TEACHING EXPERIENCE

- **Teaching Assistant in Data Struture**
	Mar. 2015 ~ Jul. 2015
- **Teaching Assistant in Principle and Pratice of Computer Algorithm**
	Jul. 2015 ~ Aug. 2015

## ADDITION

- Second debater in Zhiyuan College Debate team, 2013
- Vice-Chairman in Science Association of Zhiyuan College, 2015
- Programming Language
	C/C++, Python, Pascal, Markdown, Latex, Assembly, Java, Verilog HDL, Matlab
- Hobby
	Classical Guitar, Novel

